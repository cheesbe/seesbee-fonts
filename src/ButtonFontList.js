import React from 'react';
import ButtonFontListItem from './ButtonFontListItem';

function ButtonFontList(props) {
  const { fonts, onClick, activeFont, fontLang } = props;
  const ButtonFontListItems = fonts.map((font, index) =>
    <ButtonFontListItem
      number={index + 1}
      font={font}
      onClick={onClick}
      key={font}
      activeFont={activeFont}
      fontLang={fontLang}
    />
  );
  return (
    <div className={`list-button-fonts ${fontLang}`}>
      {ButtonFontListItems}
    </div>
  );
}

export default ButtonFontList;