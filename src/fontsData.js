class fontsData {
  constructor() {
    this.TW = ['中黑體', '中圓體', '特明體', '顏楷體', '康楷體', '歐楷體', '毛楷體', '標隸體', '豪隸體', '祥隸體', '行書體', '新篆體', '角篆體', '姚體', '粗鋼體', '少女體', '兒風體', '娃娃體', '飾藝體', '仿宋體'];

    this.EN = ['Crimson Text', 'Pacifico', 'Varela Round', 'Gloria Hallelujah', 'Maven Pro', 'Dancing Script', 'Berkshire Swash', 'PT Sans', 'Courgette', 'Rock Salt', 'Alex Brush', 'Satisfy'];
  }
}

export default new fontsData();