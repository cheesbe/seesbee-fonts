import React from 'react';
import LangListItem from './LangListItem';

function LangList(props) {
  const { langs, onClick, activeLang } = props;
  const LangListItems = langs.map(lang =>
    <LangListItem value={lang.value} name={lang.name} onClick={onClick} activeLang={activeLang} key={lang.value} />
  );
  return (
    <div className="list-langs">
      {LangListItems}
    </div>
  );
}

export default LangList;