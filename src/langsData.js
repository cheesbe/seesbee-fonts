class langsData {
  constructor() {
    this.langs = [
      {
        value: 'tw',
        name: '繁體字型',
      },
      {
        value: 'en',
        name: '英文字型',
      },
    ];
  }
}

export default new langsData();