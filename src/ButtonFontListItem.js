import React from 'react';

function ButtonFont(props){
  const { number, font, onClick, activeFont, fontLang } = props;
  const numberStr = number.toString();
  let code = '';
  if (fontLang === 'tw') {
    code = 'F';
  } else if (fontLang === 'en') {
    code = 'E';
  }
  const numberZero = ('0' + numberStr).slice(-2);
  const codeNumber = code.toLowerCase() + numberStr;
  return (
    <button
      type="button"
      name="font"
      value={codeNumber}
      onClick={onClick}
      className={activeFont === codeNumber ? 'active' : ''}
    >
      {code + numberZero}<br />{font}
    </button>
  );
}

export default ButtonFont;