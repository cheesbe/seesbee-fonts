import React, { Component } from 'react';
import { sify, tify } from 'chinese-conv';
import LangList from './LangList';
import ButtonFontList from './ButtonFontList';
import langsData from './langsData';
import fontsData from './fontsData';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '你的名字',
      font: 'f10',
      lang: 'tw',
      demoSize: '20',
      isTipShow: true,
      isNoticeShow: true,
      noticeText: '字型載入中...',
    };
    this.changeVal = this.changeVal.bind(this);
    this.handleFocus = this.handleFocus.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
  }

  componentDidMount() {
    const self = this;
    document.fonts.onloadingdone = function() {
      self.setState({
        isNoticeShow: false,
      });
    };
  }

  changeVal(e) {
    const { value, name } = e.target;
    this.setState({
      [name]: value,
    });
    if (name === 'name') {
      this.setState({
        isTipShow: false,
      });
    }

    const { lang } = this.state;
    if (name === 'font' && lang === 'tw') {
      if (!document.fonts.check('1em '+value)) {
        this.setState({
          isNoticeShow: true,
        });
      }
      const self = this;
      document.fonts.onloadingdone = function() {
        self.setState({
          isNoticeShow: false,
        });
      };
    }
  }

  handleFocus(e) {
    e.target.select();
  }

  handleBlur(e) {
    if (!e.target.value) {
      this.setState({
        name: '你的名字',
        isTipShow: true,
      });
    }
  }

  render() {
    const { font, lang, demoSize, isTipShow, isNoticeShow, noticeText } = this.state;
    let { name } = this.state;
    const cnFonts = ['f10', 'f14'];
    if (cnFonts.includes(font)){
      name = sify(name);
    } else {
      name = tify(name);
    }

    return (
      <div className="app">
        <header className="header-main">
          <h1 className="app-title">
            <a href="https://seesbee.com" target="_blank" rel="noopener noreferrer">
              <em className="f10">贴贴人</em>
            </a>
            <span>字型展示廳</span>
          </h1>
          <a href="https://seesbee.com" className="btn btn-default btn-rounded" target="_blank" rel="noopener noreferrer">貼貼人官網</a>
        </header>  
        <main className="main-main">
          <div className="box-slider">
            <input type="range" min="5" max="30" name="demoSize" value={demoSize} className="slider" onChange={this.changeVal} />
            <span className="demoSizeText">{demoSize} vw</span>
          </div>
          {isTipShow &&
            <div className="text-center">
              <label className="label-tip jump">點擊編輯</label>
            </div>
          }
          <textarea className={`demo ${font}`} name="name" style={{ fontSize: demoSize + 'vw'}} rows="1" value={name} onChange={this.changeVal} onClick={this.handleFocus} onBlur={this.handleBlur} />
        </main>
        <div className="box-fonts">
          <LangList langs={langsData.langs} onClick={this.changeVal} activeLang={lang} />
          <div className="list-fonts">
            {lang === 'tw' &&
              <ButtonFontList fonts={fontsData.TW} onClick={this.changeVal} activeFont={font} fontLang="tw" />
            }
            {lang === 'en' &&
              <ButtonFontList fonts={fontsData.EN} onClick={this.changeVal} activeFont={font} fontLang="en" /> 
            }
          </div>
        </div>
        <div className={`notice ${isNoticeShow ? 'show' : ''}`}>
          <h5 className="shake">{noticeText}</h5>
        </div>
      </div>
    );
  }
}

export default App;
