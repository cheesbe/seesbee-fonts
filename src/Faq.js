import React from 'react';

function Faq(){
  return (
    <div className="faq">
      <p>如果出現不一樣的字，代表此字型沒有收錄，建議選取別種字型喔！</p>
    </div>
  );
}

export default Faq;