import React from 'react';

function LangListItem(props){
  const { value, onClick, activeLang, name } = props;
  return (
    <button type="button" name="lang" value={value} onClick={onClick} className={activeLang === value ? 'active' : ''}>{name}</button>
  );
}

export default LangListItem;